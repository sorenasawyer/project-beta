# CarCar
Designed and Created By
_Sorena Sawyer_

## Functionality

Car Sales businesses can manage their inventory as well as their sales histories with the CarCar application. You can add new sales people, new customers, create new sales - which also removes cars from the inventory. Easily manage inventory by removing or adding new models, makes, and manufacturers.

## Technologies

CarCar is a full stack application built with Python and Django for the back end and the front end is React.js with JSX. Docker is utilized to build and maintain containers and a poller is used for updated to the inventory API. 

### Sales microservice

I created four models, the SalesPerson, Customer, SalesRecord and a VO for the Automobile (to have access to it's vin). The SalesRecord has ties to the customer, salesperson, and automobile models in this microservice. As well as a price attribute. The sales person model has a name and employee number as attributes. The customer has a name, address, and phone number as attributes. 

When a new sale is created, there are options for available automobiles via their VIN from inventory, sales persons, customers, and a price field. When an automobile is sold, it is no longer available to sell again, but is not deleted from inventory. There are separate paths to forms to do the following: create new customers, create new sales person, create a sale, create new manufacturers, models, and automobiles. As well as paths to list all previous sales and to select a sales person and see their sale records. There are also inventory paths to show all of the automobiles, the models, and manufacturers. 

The pathways are in two separate dropdowns, one for inventory and the other for sales microservice. 

## Installation

- Clone the repository down to your local machine
- CD into the new project directory
- Run docker volume create beta-data
- Run docker compose build
- Run docker compose up


## Utilize the application

If you'd like to test out the features, please go to localhost:3000/. 

Here, you can see the landing page with a drop-down navigation along the top of the browser. You can create new inventory in order to see Automobiles, Manufacturers, and Models. First, create a Manufacturer, then create a Model, and finally you can create a new Automobile with custom VIN numbers and is associated to selected Manufacturers and Models.

Then, you will be able to create new Sales people and Customers to create new sales records. Finally, you can view sales history and a chosen Sales Person's sales history. 
