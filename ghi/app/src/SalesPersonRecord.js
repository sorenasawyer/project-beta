import React from 'react';

class SalesPersonRecord extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      all_employees: [],
      employee_number: [],
      current_employee_sales: null,
    };
    this.handleSalesPersonChange = this.handleSalesPersonChange.bind(this);
    this.fetchCurrentEmployeeSales = this.fetchCurrentEmployeeSales.bind(this);
  }

  async componentDidMount() {
    const url = ('http://localhost:8090/api/sales-person/');
    const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        this.setState({all_employees: data.sales_person})
      }
  }

  handleSalesPersonChange(event) {
    const value = event.target.value;
    this.setState({employee_number: value})
    // calling the method and giving it the argument of the value which is the id of the employee chosen
    this.fetchCurrentEmployeeSales(value)
  }

  async fetchCurrentEmployeeSales(id){
    // using the id of the current_employee to fetch the data of their sales
    // update current_employee_sales with that data to be displayed in the ternary operator in our jsx
    const employeeUrl = `http://localhost:8090/api/sales/${id}/`;
    const employeeResponse = await fetch(employeeUrl);
      if (employeeResponse.ok) {
        const employeeData = await employeeResponse.json();
        this.setState({current_employee_sales: employeeData})
      }
  }
  
  // was going to use this callback function to force the change on state, but it wasn't needed
  // this.setState({current_employee_sales: employeeData}, () => {this.forceUpdate()})

  render () {
      return (
        <>
        <div className='container'>
          <h1>Sales Person History</h1>
          <div>
          <select onChange={this.handleSalesPersonChange} required name="all_employees" id="all_employees" className="form-select">
                  <option value="">Choose a Sales Person</option>
                  {this.state.all_employees.map(sales_person => {
                    return (
                      <option key={sales_person.id} value={sales_person.id}>
                        {sales_person.name}
                      </option>
                    );
                  })}
                  </select>
          </div>
        </div>
        <div className='container'>
          <table className='table table-striped'>
            <thead>
              <tr>
                <th>Sales Person</th>
                <th>Customer</th>
                <th>Vin</th>
                <th>Sale Price</th>
              </tr>
            </thead>

          {
            this.state.current_employee_sales ?             
            <tbody>
              {this.state.current_employee_sales.map((sale, idx) => {
                return (
                <tr key={idx}>
                  <td>{sale.sales_person.name}</td>
                  <td>{sale.customer.name}</td>
                  <td>{sale.vin}</td>
                  <td>{'$' + sale.price}</td>
                </tr>
                );
              })}
            </tbody>
            :
            null
          }
          </table>
        </div>
        </>
      )
  }
}



export default SalesPersonRecord;