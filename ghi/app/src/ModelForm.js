import React from 'react';

class NewModelForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
        name: '',
        manufacturers: [],
        manufacturer_id: '',
        picture_url: '',
    };
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleManufacturerChange = this.handleManufacturerChange.bind(this);
    this.handleImageChange = this.handleImageChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  async componentDidMount() {
    const url = '	http://localhost:8100/api/manufacturers/';
    const response = await fetch(url);
      if(response.ok) {
        const data = await response.json();
        this.setState({manufacturers: data.manufacturers});
      }
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    delete data.manufacturers

    const modelUrl = 'http://localhost:8100/api/models/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json'
      },
    };
    const modelResponse = await fetch(modelUrl, fetchConfig);
      if (modelResponse.ok) {
        this.setState({
          name: '',
          manufacturer_id: '',
          picture_url: '',
        });
      }
  }

  handleNameChange(event) {
    const value = event.target.value;
    this.setState({name: value})
  }

  handleManufacturerChange(event) {
    const value = event.target.value;
    this.setState({manufacturer_id: value})
  }

  handleImageChange(event) {
    const value = event.target.value;
    this.setState({picture_url: value})
  }


  render () {
    return (
      <>
      <div className="row">
      <div className="offset-3 col-6">
      <div className="shadow p-4 mt-4">
        <h1>Create a new model</h1>
          <form onSubmit={this.handleSubmit} id="create-customer-form">
            <div className="form-floating mb-3">
              <input onChange={this.handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" value={this.state.name}/>
              <label htmlFor="name">Name</label>
            </div>
            <div>
            <select onChange={this.handleManufacturerChange} name="manufacturer_id" id="manufacturer_id" required value={this.state.manufacturer_id} className="form-select">
              <option value="">Choose a manufacturer</option>
              {this.state.manufacturers.map(manufacturer_id => {
                return (
                  <option key={manufacturer_id.id} value={manufacturer_id.id}>
                    {manufacturer_id.name}
                  </option>
                );
              })}
            </select>
            </div>
            <div className="form-floating mb-3">
                <input onChange={this.handleImageChange} placeholder="Image" required type="url" name="picture_url" id="picture_url" className="form-control" value={this.state.picture_url} />
                <label htmlFor="picture_url">Image</label>
              </div>
            <button className="btn btn-primary">Create</button>
          </form>
      </div>
      </div>
      </div>
      </>
        );
    }
}

export default NewModelForm;