import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import NewCustomerForm from './CustomerForm';
import NewSalesPersonForm from './SalesPersonForm';
import ListSalesRecord from './SalesRecord';
import SalesPersonRecord from './SalesPersonRecord';
import NewSaleForm from './SalesForm';
import ListManufacturers from './ListManufacturers';
import ListModels from './ListModels';
import ListAutomobiles from './ListAutomobiles';
import NewManufacturerForm from './ManufacturerForm';
import NewModelForm from './ModelForm';
import NewAutomobileForm from './AutomobileForm';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/automobiles" element={<ListAutomobiles />} />
          <Route path="new-automobile" element={<NewAutomobileForm />} />
          <Route path="/models" element={<ListModels />} />
          <Route path="/new-model" element={<NewModelForm />} />
          <Route path="/manufacturers" element={<ListManufacturers />} />
          <Route path="/new-manufacturer" element={<NewManufacturerForm />} />
          <Route path="/customer" element={<NewCustomerForm />} />
          <Route path="/sales-person" element={<NewSalesPersonForm />} />
          <Route path="/list-sales" element={<ListSalesRecord />} />
          <Route path="/records" element={<SalesPersonRecord />} />
          <Route path="/new-sale" element={<NewSaleForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
